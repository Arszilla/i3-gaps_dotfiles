# Kali i3 | Custom ISO's dotfiles
## Disclaimer
As of 2023-01-27, `i3-gaps` is no longer maintained as it has finally merged 
with `i3`, bringing gaps to `i3` itself. Thus, this repository has been 
renamed to `i3-dotfiles` and will be maintained as such.

## Description
These are the custom dotfiles and scripts supplied on [Kali i3][Kali i3]. All 
of the files here work in unison, thus they are advised to be moved/applied 
together.

## Installation
Run the following commands to copy the dotfiles to their respective locations:

```
# Copy dotfiles to your $HOME directory:
rsync -a "$PWD"/dotfiles/etc/skel/ "$HOME"/

# Copy dotfiles to /etc/skel/:
rsync -a "$PWD"/dotfiles/etc/skel/ /etc/skel/

# Copy dotfiles to /root/:
rsync -a "$PWD"/dotfiles/etc/skel/ /root/

# Copy scripts to /usr/bin/:
rsync -a "$PWD"/dotfiles/usr/bin/ /usr/bin/
```

## Information
This repository contains dotfiles and supplementary scripts for the following programs:
- `dunst`
- `i3`
- `lxappearance`
- `nitrogen`
- `picom`
- `polybar`
- `ranger`
- `rofi`

## List of Scripts
The scripts listed and installed here are called by `/etc/skel/.config/i3/config`
- `/usr/bin/brightnessControl` for handling `brightnessctl`
- `/usr/bin/volumecontrol` for handling `amixer`/`pulseaudio`

## FAQ
To disable quasi-altering (Fibonacci) windows, comment out the following line in
`.config/i3/config.d/execs.conf`:

```
exec --no-startup-id $HOME/.config/i3/scripts.d/alternating_layouts.py
```

This can be achieved via terminal with the following command:

```
$ sed -i '/alternating_layouts.py/s/^/#/g' $HOME/.config/i3/config.d/execs.conf
```

[Kali i3]: https://gitlab.com/Arszilla/kali-i3/
